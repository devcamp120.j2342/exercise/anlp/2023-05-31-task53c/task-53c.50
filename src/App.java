import models.MovableCircle;
import models.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point1 = new MovablePoint(1, 0, 2, 2);
        System.out.println(point1);

        MovableCircle circle1 = new MovableCircle(2, point1);
        System.out.println(circle1);


        point1.moveDown();
        point1.moveDown();
        point1.moveRight();
        System.out.println(point1);


        circle1.moveUp();
        circle1.moveRight();
        System.out.println(circle1);

    }
}
